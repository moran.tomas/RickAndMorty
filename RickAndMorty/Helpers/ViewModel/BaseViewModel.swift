//
//  BaseViewModel.swift
//  RickAndMorty
//
//  Created by Tomas moran on 10/10/2023.
//

import Combine

protocol BaseViewModel {
    var state: PassthroughSubject<StateController, Never> { get }
    func viewDidLoad()
}
