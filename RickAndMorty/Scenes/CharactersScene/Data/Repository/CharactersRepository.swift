//
//  CharactersRepository.swift
//  RickAndMorty
//
//  Created by Tomas moran on 10/10/2023.
//

import Foundation

protocol CharactersRepositoryProtocol {
    func fetchCharacters(urlList: String) async throws -> (info: Info, character: [Character])
}

struct CharacterRepository: CharactersRepositoryProtocol {
    
    let apiClient: ApiClientServiceProtocol
    
    func fetchCharacters(
        urlList: String
    ) async throws -> (info: Info, character: [Character]) {
        let url = URL(string: urlList)
        return try await apiClient.request(url: url, type: ResultsCharactersDTO.self).toDomain()
    }
}
