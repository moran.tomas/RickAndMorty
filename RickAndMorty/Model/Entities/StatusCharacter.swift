//
//  StatusCharacter.swift
//  RickAndMorty
//
//  Created by Tomas moran on 10/10/2023.
//

enum StatusCharacter: String {
    case alive
    case dead
    case unknown
    
    init?(status: String?) {
        guard let status = StatusCharacter(rawValue: status?.lowercased() ?? .empty) else { return nil }
        self = status
    }
}

extension StatusCharacter {
    var description: String {
        switch self {
        case .alive:
            return Constants.statusAlive
            
        case .dead:
            return Constants.statusDead
            
        case .unknown:
            return Constants.statusUnknown
        }
    }
}
