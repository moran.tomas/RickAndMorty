//
//  Errors.swift
//  RickAndMorty
//
//  Created by Tomas moran on 04/10/2023.
//

import Foundation

enum ApiError: Error {
    case clientError
    case serverError
    case unknownError
    case errorUrl
    case errorEncoding
}

extension ApiError: LocalizedError {
    
    public var errorDescription: String? {
        switch self {
        case .clientError:
            return NSLocalizedString("Client Error", comment: "")
            
        case .serverError:
            return NSLocalizedString("server Error", comment: "")
            
        case .unknownError:
            return NSLocalizedString("Unknown Error", comment: "")
            
        case .errorUrl:
            return NSLocalizedString("Url Error", comment: "")
            
        case .errorEncoding:
            return NSLocalizedString("Encoding Error", comment: "")
        }
    }
    
}
