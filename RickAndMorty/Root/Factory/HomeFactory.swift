//
//  HomeFactory.swift
//  RickAndMorty
//
//  Created by Tomas moran on 02/10/2023.
//

import Combine
import UIKit

protocol HomeFactoryProtocol {
    func createView(coordinator: HomeViewControllerCoordinator) -> UIViewController
    func makeCharactersCoordinator(
        navigation: UINavigationController,
        urlList: String) -> Coordinator
    
    func makeLocationsCoordinator(
        navigation: UINavigationController,
        urlLocation: String) -> Coordinator
    
    func makeEpisodesCoordinator(
        navigation: UINavigationController,
        urlEpisodes: String) -> Coordinator
}

struct HomeFactory: HomeFactoryProtocol {
    
    let appContainer: AppContainerProtocol
    
    func createView(coordinator: HomeViewControllerCoordinator) -> UIViewController {
        let repository = HomeRepository(
            apiClientService: ApiClientService(),
            urlString: Endpoint.baseUrl)
        
        let loadHomeUseCase = LoadHomeCase(
            repository: repository)
        
        let state = PassthroughSubject<StateController, Never>()
        
        let homeViewModel = HomeViewModel(
            state: state,
            loadHomeCase: loadHomeUseCase)
        
        let viewController = HomeViewController(
            viewModel: homeViewModel,
            layout: makeLayout(), coordinator: coordinator)
    
        viewController.title = Constants.appName
        return viewController
    }
    
    private func makeLayout() -> UICollectionViewFlowLayout {
        let layout = UICollectionViewFlowLayout()
        let width = (ViewValues.widthScreen / ViewValues.multiplierTwo ) - ViewValues.doublePadding
        let height = (ViewValues.widthScreen / ViewValues.multiplierTwo) - ViewValues.doublePadding
        
        layout.itemSize = CGSize(
            width: width,
            height: height)
        
        layout.minimumLineSpacing = .zero
        layout.minimumLineSpacing = .zero
        layout.sectionInset = UIEdgeInsets(
            top: .zero,
            left: ViewValues.padding,
            bottom: .zero,
            right: ViewValues.padding)
        
        return layout
    }
    
    func makeCharactersCoordinator(
        navigation: UINavigationController,
        urlList: String
    ) -> Coordinator {
        let characterFactory = CharacterFactory(
            urlList: urlList,
            appContainer: appContainer)
        let characterCoordinator = CharactersCoordinator(
            navigation: navigation,
            charactersFactory: characterFactory)
        return characterCoordinator
    }
    
    func makeLocationsCoordinator(navigation: UINavigationController, urlLocation: String) -> Coordinator {
        let locationsFactory = LocationsFactory(
            urlLocations: urlLocation,
            appContainer: appContainer)
        let locationsCoordinator = LocationsCoordinator(
            locationsFactory: locationsFactory,
            navigation: navigation)
        return locationsCoordinator
    }
    
    func makeEpisodesCoordinator(
            navigation: UINavigationController, urlEpisodes: String
        ) -> Coordinator {
            let episodesFactory = EpisodesFactory(
                urlEpisodes: urlEpisodes,
                appContainer: appContainer)
            let episodesCoordinator = EpisodesCoordinator(
                episodesFactory: episodesFactory,
                navigation: navigation)
            return episodesCoordinator
        }
    
}
