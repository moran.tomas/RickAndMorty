//
//  OriginDTO.swift
//  RickAndMorty
//
//  Created by Tomas moran on 10/10/2023.
//

struct OriginDTO: Decodable {
    let name: String
    let url: String
}
