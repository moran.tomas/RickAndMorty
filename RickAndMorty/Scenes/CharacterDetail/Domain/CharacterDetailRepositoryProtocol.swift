//
//  CharacterDetailRepositoryProtocol.swift
//  RickAndMorty
//
//  Created by Tomas moran on 11/10/2023.
//

protocol CharacterDetailRepositoryProtocol {
    func fetchCharacterDetail(urlDetail: String) async throws -> Character 
}
