////
////  CharacterViewControllerTest.swift
////  RickAndMortyTests
////
////  Created by Tomas moran on 20/07/2023.
////
//
//import XCTest
//@testable import RickAndMorty
//
//// MARK: - Some Simple Test Cases
//class CharacterViewControllerTests: XCTestCase {
//
//    var characterViewController: CharacterViewController!
//    var dto: Character?
//    let characterData: [String: Any] = [
//        "id": 1,
//        "name": "Rick Sanchez",
//        "status": "Alive",
//        "species": "Human",
//        "type": "",
//        "gender": "Male",
//        "origin": [
//            "name": "Earth",
//            "url": "https://rickandmortyapi.com/api/location/1"
//        ],
//        "location": [
//            "name": "Earth",
//            "url": "https://rickandmortyapi.com/api/location/20"
//        ],
//        "image": "https://rickandmortyapi.com/api/character/avatar/1.jpeg",
//        "episode": [
//            "https://rickandmortyapi.com/api/episode/1",
//            "https://rickandmortyapi.com/api/episode/2"
//        ],
//        "url": "https://rickandmortyapi.com/api/character/1",
//        "created": "2017-11-04T18:48:46.250Z"
//    ]
//    
//
//    override func setUp() {
//        super.setUp()
//        
//        do {
//            let jsonData = try JSONSerialization.data(withJSONObject: characterData, options: .prettyPrinted)
//            print(String(data: jsonData, encoding: .utf8) ?? "Error converting JSON data to string.")
//            let decoder = JSONDecoder()
//            decoder.keyDecodingStrategy = .convertFromSnakeCase
//            self.dto = try decoder.decode(Character.self, from: jsonData)
//            
//        } catch {
//            print("Error converting dictionary to JSON data: \(error.localizedDescription)")
//        }
//        
//        characterViewController = CharacterViewController(model: dto)
//    }
//
//    override func tearDown() {
//        characterViewController = nil
//        super.tearDown()
//    }
//
//    func testViewModelConfiguration() {
//        // Given
//        let viewModel = CharacterViewModelMockSuccess()
//        let characterViewController = CharacterViewController(model: self.dto,
//                                                              viewModel: viewModel)
//
//        // When
//        let expectation = XCTestExpectation(description: "Model was well initialized")
//        characterViewController.viewDidLoad()
//
//        // Then
//        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
//            XCTAssertEqual(viewModel.characterMock?.name, self.dto?.name, "View model should be configured with the correct Name character")
//            expectation.fulfill()
//        }
//    }
//    
//    func testLoadDataSuccess() {
//        // Given
//        let viewModel = CharacterViewModelMockSuccess()
//        let characterViewController = CharacterViewController(model: self.dto,
//                                                              viewModel: viewModel)
//
//        // When
//        let expectation = XCTestExpectation(description: "Character was sent well")
//        characterViewController.viewDidLoad()
//
//        // Then
//        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
//            XCTAssertTrue(viewModel.successHasCalled == true)
//            expectation.fulfill()
//        }
//    }
//
//    func testLoadDataError() {
//        // Given
//        let viewModel = CharacterViewModelMockSuccess()
//        let characterViewController = CharacterViewController(model: nil,
//                                                              viewModel: viewModel)
//
//        // When
//        let expectation = XCTestExpectation(description: "Error alert presented")
//        characterViewController.viewDidLoad()
//
//        // Then
//        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
//            XCTAssertTrue(viewModel.errorHasCalled == true)
//            expectation.fulfill()
//        }
//    }
//}
