//
//  CharacterFactory.swift
//  RickAndMorty
//
//  Created by Tomas moran on 04/10/2023.
//

import UIKit
import Combine

protocol CharacterFactoryProtocol {
    func makeModule(coordinator: CharactersViewControllerCoordinator) -> UIViewController
    func makeCharacterDetailCoordinator(navigation: UINavigationController, urlDetail: String) -> Coordinator
}

struct CharacterFactory: CharacterFactoryProtocol {
    
    let urlList: String
    let appContainer: AppContainerProtocol
    
    func makeModule(coordinator: CharactersViewControllerCoordinator) -> UIViewController {
        let state = PassthroughSubject<StateController, Never>()
        let apiClient = appContainer.apiClient
        let characterRepository = CharacterRepository(apiClient: apiClient)
        let loadCharactersUseCase = LoadCharactersUseCaseImp(
            characterRepository: characterRepository,
            url: urlList)
        
        let lastPage = LastPageValidationUseCase()
        let viewModel = CharactersViewModel(
            loadCharactersUseCase: loadCharactersUseCase,
            lastPageCharactersUseCase: lastPage,
            imageDataUseCase: appContainer.getDataImageUseCase(),
            state: state)
        
        let controller = CharactersViewController(
            viewModel: viewModel,
            coordinator: coordinator)
        controller.title = "Characters"
        return controller
    }
    
    func makeCharacterDetailCoordinator(
        navigation: UINavigationController, urlDetail: String) -> Coordinator {
            let characterDetailFactory = CharacterDetailFactory(
                urlDetail: urlDetail,
                appContainer: appContainer)
            
            let characterDetailCoordinator = CharacterDetailCoordinator(
                navigation: navigation,
                characterDetailFactory: characterDetailFactory)
            return characterDetailCoordinator
    }
    
}

