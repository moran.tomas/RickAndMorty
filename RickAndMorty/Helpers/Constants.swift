//
//  Constants.swift
//  RickAndMorty
//
//  Created by Tomas moran on 03/10/2023.
//

struct Constants {
    static let logoRickMorty = "logoRickMorty"
    static let titleLogoRickMorty = "titleLogoRickMorty"
    static let descriptionLabel = "Characters"
    static let id = "ID"
    static let name = "Name"
    static let status = "Status"
    static let species = "Species"
    static let type = "Type"
    static let gender = "Gender"
    static let origin = "Origin"
    static let location = "Location"
    static let created = "Created"
    static let error = "Error"
    static let appName = "Rick And Morty"
    
    static let statusAlive = "Status: 🟢 Alive"
    static let statusDead = "Status: 🪦 Dead"
    static let statusUnknown = "Status: ❓ Unknown"
    
    static let alien = "Alien"
    static let human = "Human"
    static let unknown = "Unknown"
    
    static let specieHuman = "Specie: 👤 Human"
    static let specieAlien = "Specie: 👽 Alien"
    static let specie = "Specie:"
    
    static let locations = "Locations"
}

struct Images {
    static let defaultImage = "default"
}
