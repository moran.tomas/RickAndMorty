////
////  CharacterViewModelMock.swift
////  RickAndMortyTests
////
////  Created by Tomas moran on 20/07/2023.
////
//
//import XCTest
//@testable import RickAndMorty
//
//class CharacterViewModelMockSuccess {
//    
//    // Variables
//    private(set) var model: Character?
//    
//    // Closures
//    var sendCharacter: ((Character) -> Void)?
//    var showError: ((String) -> Void)?
//    
//    var errorHasCalled: Bool = false
//    var successHasCalled: Bool = false
//    var characterMock: Character? = nil
//    
//    init() { }
//    
//}
//
//extension CharacterViewModelMockSuccess: CharacterViewModelProtocol {
//    
//    func setModel(model: RickAndMorty.Character?) {
//        self.model = model
//        characterMock = model
//    }
//    
//    func loadData() {
//        guard let character = self.model else {
//            errorHasCalled = true
//            showError?("TEST")
//            return
//        }
//        successHasCalled = true
//        sendCharacter?(character)
//    }
//}
//
//
//
