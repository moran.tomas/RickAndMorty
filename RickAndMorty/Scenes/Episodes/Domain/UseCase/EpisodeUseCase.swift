//
//  EpisodeUseCase.swift
//  RickAndMorty
//
//  Created by Tomas moran on 14/10/2023.
//

import Foundation

protocol LoadEpisodesUseCaseProtocol {
    mutating func getEpisodes() async -> Result<[Episode], Error>
}

struct LoadEpisodesUseCase: LoadEpisodesUseCaseProtocol {
    
    private(set) var episodesRepository: EpisodesRepositoryProtocol
    private(set) var urlEpisodes: String
    
    mutating func getEpisodes() async -> Result<[Episode], Error> {
        guard !urlEpisodes.isEmpty else { return .success([])}
        do {
            let result = try await episodesRepository
                .fetch(urlEpisodes: urlEpisodes)
            urlEpisodes = result.info.next ?? .empty
            return .success(result.episodes)
        }catch {
            return .failure(error)
        }
    }
}
