//
//  HttpResponseStatus.swift
//  RickAndMorty
//
//  Created by Tomas moran on 04/10/2023.
//

enum HttpResponseStatus {
    static let ok = 200...299
    static let clienteError = 400...499
    static let serverError = 500...599
}
