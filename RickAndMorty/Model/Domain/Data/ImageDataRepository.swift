//
//  ImageDataRepository.swift
//  RickAndMorty
//
//  Created by Tomas moran on 11/10/2023.
//

import Foundation

struct ImageDataRepository: ImageDataRepositoryProtocol {
    
    private(set) var remoteDataService: RemoteImageDataServiceProtocol
    private(set) var localDataCache: LocalDataImageServiceProtocol
    
    func fetchData(url: URL?) async -> Data? {
        let data =  await remoteDataService.request(url: url)
        localDataCache.save(key: url?.absoluteString ?? .empty, data: data)
        return data
    }
    
    func getFromCache(url: String?) -> Data? {
        localDataCache.get(key: url ?? .empty)
    }
}
