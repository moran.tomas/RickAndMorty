//
//  Factory.swift
//  RickAndMorty
//
//  Created by Tomas moran on 02/10/2023.
//

import UIKit

protocol AppFactoryProtocol {
    func setupHomeCoordinator(navigation: UINavigationController) -> Coordinator
}

struct AppFactory: AppFactoryProtocol {

    let appContainer = AppContainer()
    
    func setupHomeCoordinator(navigation: UINavigationController) -> Coordinator {
        let homeFactory = HomeFactory(appContainer: appContainer)
        let homeCoordinator = HomeCoordinator(navigation: navigation, homeFactory: homeFactory)
        return homeCoordinator
    }
    
}
