//
//  OriginCoordinator.swift
//  RickAndMorty
//
//  Created by Tomas moran on 13/10/2023.
//

import UIKit

final class OriginCoordinator: Coordinator {
    
    private var originFactory: OriginFactoryProtocol
    var navigation: UINavigationController
    
    init(navigation: UINavigationController, originFactory: OriginFactoryProtocol) {
        self.navigation = navigation
        self.originFactory = originFactory
    }
    
    func start() {
        let controller = originFactory.makeModule()
        navigation.pushViewController(controller, animated: true)
    }
}
