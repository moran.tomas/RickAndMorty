//
//  AppCoordinator.swift
//  RickAndMorty
//
//  Created by Tomas moran on 02/10/2023.
//

import UIKit

final class AppCoordinator: Coordinator {
     
    var navigation: UINavigationController
    private let appFactory: AppFactory
    private var homeCoordinator: Coordinator?
    
    init(navigation: UINavigationController, appFactory: AppFactory, window: UIWindow?) {
        self.navigation = navigation
        self.appFactory = appFactory
        setupWindow(window: window)
    }
    
    func start() {
        homeCoordinator = appFactory.setupHomeCoordinator(navigation: navigation)
        homeCoordinator?.start()
    }
    
    func setupWindow(window: UIWindow?) {
        window?.rootViewController = navigation
        window?.makeKeyAndVisible()
    }
    
}


