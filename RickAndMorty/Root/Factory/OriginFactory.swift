//
//  OriginFactory.swift
//  RickAndMorty
//
//  Created by Tomas moran on 13/10/2023.
//

import UIKit

protocol OriginFactoryProtocol {
    func makeModule() -> UIViewController
}

struct OriginFactory: OriginFactoryProtocol {
    func makeModule() -> UIViewController {
        let controller = OriginViewController()
        controller.title = "Origin"
        return controller
    }
}
