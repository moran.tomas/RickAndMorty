//
//  InfoDTO.swift
//  RickAndMorty
//
//  Created by Tomas moran on 10/10/2023.
//

struct InfoDTO: Decodable {
    let next: String?
}
