//
//  EpisodesViewModel.swift
//  RickAndMorty
//
//  Created by Tomas moran on 14/10/2023.
//

import Combine

protocol EpisodesViewModelProtocol: BaseViewModel {
    var itemsEpisodesCount: Int { get }
    var lastPage: Bool { get }
    func getItemEpisodeViewModel(row: Int) -> ItemEpisodeViewModel
}

final class EpisodesViewModel: EpisodesViewModelProtocol {
    
    var itemsEpisodesCount: Int {
        episodes.count
    }
    
    var lastPage: Bool {
        lastPageValidationUseCase.lastPage
    }
    
    var state: PassthroughSubject<StateController, Never>
    
    // MARK: - Private properties
    private var loadEpisodesUseCase: LoadEpisodesUseCaseProtocol
    private var lastPageValidationUseCase: LastPageValidationUseCaseProtocol
    private var episodes: [Episode] = []
    
    // MARK: - Life Cycle
    
    init(
        state: PassthroughSubject<StateController, Never>,
        loadEpisodesUseCase: LoadEpisodesUseCaseProtocol,
        lastPageValidationUseCase: LastPageValidationUseCaseProtocol
    ) {
        self.state = state
        self.loadEpisodesUseCase = loadEpisodesUseCase
        self.lastPageValidationUseCase = lastPageValidationUseCase
    }
    func viewDidLoad() {
        state.send(.loading)
        Task {
            let result = await loadEpisodesUseCase.getEpisodes()
            switch result {
            case .success(let episodesResult):
                lastPageValidationUseCase.updateLastPage(itemsCount: episodesResult.count)
                episodes.append(contentsOf: episodesResult)
                state.send(.success)
            case .failure(let error):
                state.send(.fail(error: error.localizedDescription))
            }
        }
    }

    // MARK: - Helpers
    func getItemEpisodeViewModel(row: Int) -> ItemEpisodeViewModel {
        lastPageValidationUseCase.checkAndLoadMoreItems(
            row: row, actualItems: episodes.count,
            action: viewDidLoad)
        
        let episode = episodes[row]
        return ItemEpisodeViewModel(episode: episode)
    }
}
