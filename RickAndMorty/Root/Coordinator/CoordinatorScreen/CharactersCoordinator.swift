//
//  CharactersCoordinator.swift
//  RickAndMorty
//
//  Created by Tomas moran on 04/10/2023.
//

import UIKit

final class CharactersCoordinator: Coordinator {
    var navigation: UINavigationController
    private var charactersFactory: CharacterFactoryProtocol
    
    init(navigation: UINavigationController, charactersFactory: CharacterFactoryProtocol) {
        self.navigation = navigation
        self.charactersFactory = charactersFactory
    }
    
    func start() {
        let controller = charactersFactory.makeModule(coordinator: self)
        navigation.navigationBar.prefersLargeTitles = true
        navigation.pushViewController(controller, animated: true)
    }
    
}

extension CharactersCoordinator: CharactersViewControllerCoordinator {
    func didSelectMenuCell(urlDetail: String) {
        let characterDetailCoordinator = charactersFactory.makeCharacterDetailCoordinator(
            navigation: navigation,
            urlDetail: urlDetail)
        characterDetailCoordinator.start()
    }
}
