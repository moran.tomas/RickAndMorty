//
//  ApiClientServiceProtocol.swift
//  RickAndMorty
//
//  Created by Tomas moran on 11/10/2023.
//

import UIKit

protocol ApiClientServiceProtocol {
    func request<T: Decodable>(url: URL?, type: T.Type) async throws -> T
}
