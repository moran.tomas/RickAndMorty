//
//  ResultLocationsDTO+Mapper.swift
//  RickAndMorty
//
//  Created by Tomas moran on 13/10/2023.
//

import Foundation

extension ResultLocationsDTO {
    func toDomain() -> (info: Info, locations: [Location]) {
        let info = Info(next: info.next)
        let locations = results.map {
            Location(
                name: $0.name,
                type: $0.type,
                dimension: $0.dimension,
                url: $0.url)
        }
        
        return (info: info, locations: locations)
    }
}
