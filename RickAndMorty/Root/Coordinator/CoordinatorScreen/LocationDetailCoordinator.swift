//
//  LocationDetailCoordinator.swift
//  RickAndMorty
//
//  Created by Tomas moran on 13/10/2023.
//

import UIKit

final class LocationDetailCoordinator: Coordinator {
    
    private var locationDetailFactory: LocationDetailFactoryProtocol
    var navigation: UINavigationController
    
    init(navigation: UINavigationController, locationDetailFactory: LocationDetailFactoryProtocol) {
        self.navigation = navigation
        self.locationDetailFactory = locationDetailFactory
    }
    
    func start() {
        let controller = locationDetailFactory.makeModule()
        navigation.pushViewController(controller, animated: true)
    }
}
