//
//  ImageDataRepositoryProtocol.swift
//  RickAndMorty
//
//  Created by Tomas moran on 11/10/2023.
//

import Foundation

protocol ImageDataRepositoryProtocol {
    func fetchData(url: URL?) async -> Data?
    func getFromCache(url: String?) -> Data?
}
