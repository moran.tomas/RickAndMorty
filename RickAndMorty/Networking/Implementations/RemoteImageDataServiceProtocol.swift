//
//  RemoteImageDataServiceProtocol.swift
//  RickAndMorty
//
//  Created by Tomas moran on 11/10/2023.
//

import Foundation

protocol RemoteImageDataServiceProtocol {
    func request(url: URL?) async -> Data?
}
