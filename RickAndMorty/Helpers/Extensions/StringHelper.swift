//
//  StringHelper.swift
//  RickAndMorty
//
//  Created by Tomas moran on 20/07/2023.
//

import UIKit

extension String {
    func convertTimestampToDateString() -> String? {
        // Create a date formatter
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ" // Match the timestamp format

        // Convert the timestamp string to a Date object
        if let date = dateFormatter.date(from: self) {
            // Create a new date formatter for the desired output format
            let outputFormatter = DateFormatter()
            outputFormatter.dateFormat = "MMM d, yyyy - HH:mm:ss" // Customize the output format

            // Format the Date object as a string
            let dateString = outputFormatter.string(from: date)
            return dateString
        } else {
            // Failed to convert the timestamp
            return nil
        }
    }
    
    static var empty: String {
        return String()
    }
}
