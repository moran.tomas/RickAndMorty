//
//  LocalDataImageService.swift
//  RickAndMorty
//
//  Created by Tomas moran on 11/10/2023.
//

import Foundation

protocol LocalDataImageServiceProtocol {
    func save(key: String, data: Data?)
    func get(key: String) -> Data?
}

struct LocalDataImageService: LocalDataImageServiceProtocol {
    private var dataStorage = NSCache<NSString, NSData>()
    
    func save(key: String, data: Data?) {
        guard let  data = data else { return }
        dataStorage.setObject(data as NSData, forKey: key as NSString)
    }
    
    func get(key: String) -> Data? {
        dataStorage.object(forKey: key as NSString) as? Data
    }
    
}
