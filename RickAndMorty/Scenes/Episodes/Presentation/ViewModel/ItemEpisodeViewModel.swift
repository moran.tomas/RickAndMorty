//
//  ItemEpisodeViewModel.swift
//  RickAndMorty
//
//  Created by Tomas moran on 14/10/2023.
//

import Foundation

struct ItemEpisodeViewModel {
    
    private(set) var episode: Episode
    
    var numberEpisode: String {
        "# \(episode.id)"
    }
    
    var name: String {
        episode.name
    }
    
    var airDate: String {
        episode.airDate
    }
    
    var seasonAndEpisode: String {
        episode.episode
    }
    
}
