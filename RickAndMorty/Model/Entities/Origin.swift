//
//  Origin.swift
//  RickAndMorty
//
//  Created by Tomas moran on 10/10/2023.
//

struct Origin {
    let name: String
    let url: String
}
