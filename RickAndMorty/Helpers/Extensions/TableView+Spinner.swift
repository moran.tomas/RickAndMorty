//
//  TableView+Spinner.swift
//  RickAndMorty
//
//  Created by Tomas moran on 10/10/2023.
//

import UIKit

extension UITableViewController {
    func addSpinnerLastCell() {
        let spinner = UIActivityIndicatorView(style: .medium)
        spinner.startAnimating()
        spinner.frame = CGRect(x: .zero,
                               y: .zero,
                               width: tableView.bounds.width,
                               height: CGFloat(ViewValues.defaultHeightCell))
        tableView.tableFooterView = spinner
    }
}
