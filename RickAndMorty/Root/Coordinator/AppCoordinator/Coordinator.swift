//
//  Coordinator.swift
//  RickAndMorty
//
//  Created by Tomas moran on 02/10/2023.
//

import UIKit

protocol Coordinator {
    var navigation: UINavigationController { get }
    func start()
}
