//
//  EpisodesRepository.swift
//  RickAndMorty
//
//  Created by Tomas moran on 14/10/2023.
//

import Foundation

protocol EpisodesRepositoryProtocol {
    func fetch(urlEpisodes: String)
    async throws -> (info: Info, episodes: [Episode])
}

struct EpisodesRepository: EpisodesRepositoryProtocol {
    
    private(set) var remoteService: ApiClientServiceProtocol
    
    func fetch(
        urlEpisodes: String
    ) async throws -> (info: Info, episodes: [Episode]) {
        let url = URL(string: urlEpisodes)
        return try await remoteService.request(
            url: url,
            type: ResultEpisodesDTO.self)
        .toDomain()
    }
    
}
