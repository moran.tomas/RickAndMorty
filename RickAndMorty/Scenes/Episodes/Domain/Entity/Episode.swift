//
//  Episode.swift
//  RickAndMorty
//
//  Created by Tomas moran on 14/10/2023.
//

import Foundation

struct Episode {
    let id: Int
    let name: String
    let airDate: String
    let episode: String
}
