//
//  EpisodesFactory.swift
//  RickAndMorty
//
//  Created by Tomas moran on 13/10/2023.
//

import UIKit
import Combine

protocol EpisodesFactoryProtocol {
    func makeModule() -> UIViewController
}

struct EpisodesFactory: EpisodesFactoryProtocol {
    
    private(set) var urlEpisodes: String
    private(set) var appContainer: AppContainerProtocol
    
    func makeModule() -> UIViewController {
        let state = PassthroughSubject<StateController, Never>()
        let episodesRepository = EpisodesRepository(
            remoteService: appContainer.apiClient)
        
        let loadEpisodesUseCase = LoadEpisodesUseCase(
            episodesRepository: episodesRepository,
            urlEpisodes: urlEpisodes)
        
        let lastPageValidationUseCase = LastPageValidationUseCase()
        
        let viewModel = EpisodesViewModel(
            state: state,
            loadEpisodesUseCase: loadEpisodesUseCase,
            lastPageValidationUseCase: lastPageValidationUseCase)
        
        let controller = EpisodesViewController(viewModel: viewModel)
        controller.title = "Episodes"
        return controller
    }
}
