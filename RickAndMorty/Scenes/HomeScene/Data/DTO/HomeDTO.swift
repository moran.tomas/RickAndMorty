//
//  HomeDTO.swift
//  RickAndMorty
//
//  Created by Tomas moran on 04/10/2023.
//

struct HomeDTO: Decodable {
    let characters: String
    let locations: String
    let episodes: String
}

extension HomeDTO: PropertyIterator { }
