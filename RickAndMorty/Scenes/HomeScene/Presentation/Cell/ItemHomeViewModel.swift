//
//  ItemHomeViewModel.swift
//  RickAndMorty
//
//  Created by Tomas moran on 03/10/2023.
//

struct ItemHomeViewModel {
    
    private let menuItem: HomeItem
    
    init(menuItem: HomeItem) {
        self.menuItem = menuItem
    }
    
    var title: String {
        menuItem.title.capitalized
    }
    
    var imageName: String {
        menuItem.title
    }
    
}
