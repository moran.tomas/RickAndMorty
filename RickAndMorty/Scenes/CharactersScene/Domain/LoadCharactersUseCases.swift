//
//  LoadCharactersUseCases.swift
//  RickAndMorty
//
//  Created by Tomas moran on 10/10/2023.
//

protocol LoadCharactersUseCase {
    func execute() async -> Result<[Character], Error>
}

final class LoadCharactersUseCaseImp: LoadCharactersUseCase {
    
    private let characterRepository: CharactersRepositoryProtocol
    private var url: String
    private var result: (info: Info, characters: [Character])?
    
    init(characterRepository: CharactersRepositoryProtocol, url: String){
        self.characterRepository = characterRepository
        self.url = url
    }
    
    func execute() async -> Result<[Character], Error> {
        guard !url.isEmpty else { return .success([]) }
        do {
            let repositoryResult = try await characterRepository.fetchCharacters(urlList: url)
            url = repositoryResult.info.next ?? .empty
            return .success(repositoryResult.character)
        }catch {
            return .failure(error)
        }
    }
}
