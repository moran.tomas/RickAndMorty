//
//  Character.swift
//  RickAndMorty
//
//  Created by Tomas moran on 10/10/2023.
//

struct Character {
    let id: Int
    let name: String
    let status: StatusCharacter?
    let specie: Specie
    let urlCharacter: String
    let urlImage: String?
    let origin: Origin
    let location: Location
}
