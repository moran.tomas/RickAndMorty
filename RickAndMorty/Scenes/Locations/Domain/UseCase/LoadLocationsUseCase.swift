//
//  LoadLocationsUseCase.swift
//  RickAndMorty
//
//  Created by Tomas moran on 13/10/2023.
//


protocol LoadLocationsUseCaseProtocol {
    mutating func execute() async -> Result<[Location], Error>
}

struct LoadLocationsUseCase: LoadLocationsUseCaseProtocol {
    private(set) var locationsRepository: LocationsRepository
    private(set) var urlLocations: String
    
    mutating func execute() async -> Result<[Location], Error> {
        guard !urlLocations.isEmpty else { return .success([])}

        do {
            let result = try await locationsRepository
                .fetchLocations(urlLocations: urlLocations)
            urlLocations = result.info.next ?? .empty
            return .success(result.locations)
        }catch {
            return .failure(error)
        }
    }
}
