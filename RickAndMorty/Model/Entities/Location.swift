//
//  Location.swift
//  RickAndMorty
//
//  Created by Tomas moran on 10/10/2023.
//

struct Location {
    let name: String
    let type: String?
    let dimension: String?
    let url: String
}
