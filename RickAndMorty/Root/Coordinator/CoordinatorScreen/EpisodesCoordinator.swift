//
//  EpisodesCoordinator.swift
//  RickAndMorty
//
//  Created by Tomas moran on 13/10/2023.
//

import UIKit

final class EpisodesCoordinator: Coordinator {
    private let episodesFactory: EpisodesFactoryProtocol
    var navigation: UINavigationController
    
    init(
        episodesFactory: EpisodesFactoryProtocol,
        navigation: UINavigationController
    ) {
        self.episodesFactory = episodesFactory
        self.navigation = navigation
    }
    
    func start() {
        let controller = episodesFactory.makeModule()
        navigation.pushViewController(controller, animated: true)
        navigation.navigationBar.prefersLargeTitles = true
    }
}
