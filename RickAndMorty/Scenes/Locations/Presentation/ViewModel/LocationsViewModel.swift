//
//  LocationsViewModel.swift
//  RickAndMorty
//
//  Created by Tomas moran on 13/10/2023.
//

import Combine

protocol LocationViewModelProtocol: BaseViewModel {
    var itemsLocationCount: Int { get }
    var lastPage: Bool { get }
    func getItemLocationViewModel(row: Int) -> ItemLocationViewModel
}

final class LocationViewModel: LocationViewModelProtocol {
    
    var itemsLocationCount: Int {
        locations.count
    }
    
    var lastPage: Bool {
        lastPageUseCase.lastPage
    }
    
    var state: PassthroughSubject<StateController, Never>
    
    // MARK: - Private properties
    private var loadLocationUseCase: LoadLocationsUseCaseProtocol
    private var lastPageUseCase: LastPageValidationUseCaseProtocol
    private var locations: [Location] = []
    // MARK: - Life Cycle
    init(
        state: PassthroughSubject<StateController, Never>,
        loadLocationUseCase: LoadLocationsUseCaseProtocol,
        lastPageUseCase: LastPageValidationUseCaseProtocol
    ){
        self.state = state
        self.loadLocationUseCase = loadLocationUseCase
        self.lastPageUseCase = lastPageUseCase
    }
    
    func viewDidLoad() {
        state.send(.loading)
        Task { [weak self] in
            let result = await loadLocationUseCase.execute()
            switch result {
            case .success(let locations):
                lastPageUseCase.updateLastPage(itemsCount: locations.count)
                self?.locations.append(contentsOf: locations)
                state.send(.success)
            case .failure(let error):
                state.send(.fail(error: error.localizedDescription))
            }
        }
    }
    
    // MARK: - Helpers
    func getItemLocationViewModel(row: Int) -> ItemLocationViewModel {
        checkAndLoadMoreItems(row: row)
        let location = locations[row]
        let itemLocationViewModel = ItemLocationViewModel(location: location)
        return itemLocationViewModel
    }
    
    private func checkAndLoadMoreItems(row: Int) {
        lastPageUseCase.checkAndLoadMoreItems(
            row: row, actualItems: locations.count,
            action: viewDidLoad)
    }
}
