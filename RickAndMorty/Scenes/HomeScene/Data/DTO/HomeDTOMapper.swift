//
//  HomeDTOMapper.swift
//  RickAndMorty
//
//  Created by Tomas moran on 04/10/2023.
//

extension HomeDTO {
    
    func toDomain() -> [HomeItem] {
        return self.dictionaryProperties().map { dictionary in
            let title = dictionary.key
            let url: String = (dictionary.value as? String) ?? String()
            return HomeItem(title: title, url: url)
        }
    }
    
}
