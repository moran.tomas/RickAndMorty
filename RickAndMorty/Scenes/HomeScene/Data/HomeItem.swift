//
//  HomeItem.swift
//  RickAndMorty
//
//  Created by Tomas moran on 03/10/2023.
//

struct HomeItem {
    let title: String
    let url: String
}
