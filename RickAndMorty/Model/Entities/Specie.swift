//
//  Specie.swift
//  RickAndMorty
//
//  Created by Tomas moran on 10/10/2023.
//

enum Specie {
    case alien
    case human
    case other(type: String?)
    
    init(specie: String?) {
        switch specie {
        case Constants.alien:
            self = .alien
            
        case Constants.human:
            self = .human
            
        default:
            self = .other(type: specie)
        }
    }
}

extension Specie {
    var description: String {
        switch self {
        case .alien:
            return Constants.specieAlien
        case .human:
            return Constants.specieHuman
        case .other(type: let specie):
            let typeSpecie = specie ?? Constants.unknown
            return "\(Constants.specie) \(typeSpecie)"
        }
    }
}
