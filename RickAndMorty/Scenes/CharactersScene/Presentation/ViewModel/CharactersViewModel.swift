//
//  CharactersViewModel.swift
//  RickAndMorty
//
//  Created by Tomas moran on 10/10/2023.
//

import Combine

protocol CharactersViewModelProtocol: BaseViewModel {
    var itemCharactersCount: Int { get }
    var lastPage: Bool { get }
    func getItemMenuViewModel(row: Int) -> ItemCharacterViewModel
    func getUrlDetail(row: Int) -> String
}

final class CharactersViewModel: CharactersViewModelProtocol {
    
    var state: PassthroughSubject<StateController, Never>
    
    var lastPage: Bool {
        lastPageCharactersUseCase.lastPage
    }
    
    var itemCharactersCount: Int {
        characters.count
    }
    
    private var characters: [Character] = []
    private let loadCharactersUseCase: LoadCharactersUseCase
    private var lastPageCharactersUseCase: LastPageValidationUseCaseProtocol
    private var imageDataUseCase: ImageDataUseCaseProtocol
    
    init(
        loadCharactersUseCase: LoadCharactersUseCase,
        lastPageCharactersUseCase: LastPageValidationUseCaseProtocol,
        imageDataUseCase: ImageDataUseCaseProtocol,
        state: PassthroughSubject<StateController, Never>
    ) {
        self.loadCharactersUseCase = loadCharactersUseCase
        self.lastPageCharactersUseCase = lastPageCharactersUseCase
        self.imageDataUseCase = imageDataUseCase
        self.state = state
    }
    
    func viewDidLoad() {
        state.send(.loading)
        Task {
           await loadCharactersUseCase()
        }
    }
    
    private func loadCharactersUseCase() async{
        let resultUseCase = await loadCharactersUseCase.execute()
        updateStateUI(resultUseCase: resultUseCase)
    }
    
    private func updateStateUI(resultUseCase: Result<[Character], Error>) {
        switch resultUseCase {
        case .success(let charactersArray):
            lastPageCharactersUseCase.updateLastPage(itemsCount: charactersArray.count)
            characters.append(contentsOf: charactersArray)
            state.send(.success)
        case .failure(let error):
            state.send(.fail(error: error.localizedDescription))
        }
    }
    
    func getItemMenuViewModel(row: Int) -> ItemCharacterViewModel {
        checkAndLoadMoreCharacters(row: row)
        return makeItemCharacter(row: row)
    }
    
    private func checkAndLoadMoreCharacters(row: Int) {
        lastPageCharactersUseCase.checkAndLoadMoreItems(
            row: row,
            actualItems: characters.count,
            action: viewDidLoad)
    }
    
    private func makeItemCharacter(row: Int) -> ItemCharacterViewModel {
        let character = characters[row]
        return ItemCharacterViewModel(
            character: character,
            dataImageUseCase: imageDataUseCase)
    }
    
    func getUrlDetail(row: Int) -> String {
        let character = characters[row]
        return character.urlCharacter
    }
    
}
