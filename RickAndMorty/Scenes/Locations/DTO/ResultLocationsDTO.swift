//
//  ResultLocationsDTO.swift
//  RickAndMorty
//
//  Created by Tomas moran on 13/10/2023.
//

import Foundation

struct ResultLocationsDTO: Decodable {
    let info: InfoDTO
    let results: [LocationDTO]
}
