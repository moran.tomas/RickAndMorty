//
//  ItemLocationTableViewCell.swift
//  RickAndMorty
//
//  Created by Tomas moran on 13/10/2023.
//

import UIKit

final class ItemLocationTableViewCell: UITableViewCell {
    
    private lazy var nameLabel: UILabel = makeLabels(for: .headline)
    private lazy var dimensionLabel: UILabel = makeLabels(for: .subheadline)
    private lazy var typeLabel: UILabel = makeLabels(for: .footnote)
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configUserInterface()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configUserInterface() {
        selectionStyle = .none
        
        let stack = UIStackView(
            arrangedSubviews: [nameLabel, dimensionLabel, typeLabel])
        stack.axis = .vertical
        stack.spacing = ViewValues.smallPadding
        
        addSubview(stack)
        stack.setConstraints(
        top: topAnchor,
        right: rightAnchor,
        bottom: bottomAnchor,
        left: leftAnchor,
        pTop: ViewValues.padding,
        pRight: ViewValues.padding,
        pBottom: ViewValues.padding,
        pLeft: ViewValues.doublePadding)
    }
    
    public func configData(viewModel: ItemLocationViewModel) {
        nameLabel.text = viewModel.name
        dimensionLabel.text = viewModel.dimension
        typeLabel.text = viewModel.type
    }
    
    private func makeLabels(for textStyle: UIFont.TextStyle) -> UILabel {
        let label = UILabel()
        label.font = UIFont.preferredFont(forTextStyle: textStyle)
        return label
    }
    
}

extension ItemLocationTableViewCell: Reusable { }

