//
//  ImageDataUseCase.swift
//  RickAndMorty
//
//  Created by Tomas moran on 11/10/2023.
//

import Foundation

protocol ImageDataUseCaseProtocol {
    func getData(url: URL?) async -> Data?
    func getDataFromCache(url: String?) -> Data?
}

struct ImageDataUseCase: ImageDataUseCaseProtocol {
    
    private(set) var imageDataRepository: ImageDataRepositoryProtocol
    
    func getData(url: URL?) async -> Data? {
        await imageDataRepository.fetchData(url: url)
    }
    
    func getDataFromCache(url: String?) -> Data? {
        imageDataRepository.getFromCache(url: url)
    }
}
