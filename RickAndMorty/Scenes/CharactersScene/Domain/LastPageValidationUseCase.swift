//
//  LastPageValidationUseCase.swift
//  RickAndMorty
//
//  Created by Tomas moran on 10/10/2023.
//

protocol LastPageValidationUseCaseProtocol {
    var lastPage: Bool { get }
    mutating func updateLastPage(itemsCount: Int)
    func checkAndLoadMoreItems(
        row: Int,
        actualItems: Int,
        action: () -> Void)
}

struct LastPageValidationUseCase: LastPageValidationUseCaseProtocol {
    private var threshold = 5
    var lastPage: Bool = false
    
    mutating func updateLastPage(itemsCount: Int) {
        if itemsCount == Int.zero {
            lastPage = true
        }
    }
    
    func checkAndLoadMoreItems(
        row: Int,
        actualItems: Int,
        action: () -> Void
    ) {
        guard !lastPage else { return }
        let limit = actualItems - threshold
        if limit == row {
            action()
        }
    }
}
