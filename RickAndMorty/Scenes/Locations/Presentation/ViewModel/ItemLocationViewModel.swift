//
//  ItemLocationViewModel.swift
//  RickAndMorty
//
//  Created by Tomas moran on 13/10/2023.
//

struct ItemLocationViewModel {
    
    private(set) var location: Location
    
    var name: String {
        location.name
    }
    
    var dimension: String {
        let safeDimension = location.dimension ?? Constants.unknown
        return "Dimension: \(safeDimension)"
    }
    
    var type: String {
        let safeType = location.type ?? Constants.unknown
        return "Type: \(safeType)"
    }
    
    
    
}
