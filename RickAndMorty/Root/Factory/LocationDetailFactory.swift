//
//  LocationDetailFactory.swift
//  RickAndMorty
//
//  Created by Tomas moran on 13/10/2023.
//

import UIKit

protocol LocationDetailFactoryProtocol {
    func makeModule() -> UIViewController
}

struct LocationDetailFactory: LocationDetailFactoryProtocol {
    func makeModule() -> UIViewController {
        let controller = LocationDetailViewController()
        controller.title = "Location detail"
        return controller
    }
}
