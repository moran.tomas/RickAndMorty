//
//  UIImageViewHelper.swift
//  RickAndMorty
//
//  Created by Tomas moran on 17/07/2023.
//

import UIKit

extension UIImageView {
    func downloaded(from link: String, completion: ((Bool) -> Void)? = nil) {
        if let imageURL = URL(string: link) {
            // Fetch the image data from the URL
            URLSession.shared.dataTask(with: imageURL) { (data, response, error) in
                if let error = error {
                    print("Error fetching image: \(error)")
                    return
                }
                
                // Check if there is image data
                if let imageData = data {
                    // Create a UIImage from the image data on the main thread
                    DispatchQueue.main.async {
                        if let image = UIImage(data: imageData) {
                            // Set the UIImage to the UIImageView
                            self.image = image
                        } else {
                            print("Invalid image data")
                        }
                    }
                } else {
                    print("No image data received")
                }
            }.resume()
        } else {
            print("Invalid URL")
        }
    }
    
    func setImage(_ url: String?) {
        guard let url = url else { return }
        self.downloaded(from: url)
    }
}

extension UIImageView {
    func setImageFromData(data: Data?) {
        if let data = data {
            if let image = UIImage(data: data) {
                self.image = image
            }
        }
    }
    
    func addDefaultImage() {
        image = UIImage(named: "default")
    }
}
