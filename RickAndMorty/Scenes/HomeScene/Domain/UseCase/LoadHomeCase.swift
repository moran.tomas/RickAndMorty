//
//  LoadHomeCase.swift
//  RickAndMorty
//
//  Created by Tomas moran on 03/10/2023.
//

protocol LoadHomeCaseProtocol {
    func execute() async -> Result<[HomeItem], Error>
}

struct LoadHomeCase: LoadHomeCaseProtocol {
    
    let repository: HomeRepositoryProtocol
    
    func execute() async -> Result<[HomeItem], Error> {
        do {
            let result = try await repository.fetchData()
            return .success(result)
        } catch {
            return .failure(error)
        }
    }
    
}
