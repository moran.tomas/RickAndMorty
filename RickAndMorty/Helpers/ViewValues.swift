//
//  ViewValues.swift
//  RickAndMorty
//
//  Created by Tomas moran on 04/10/2023.
//

import UIKit

struct ViewValues {
    static let tagIdentifierSpinner = 123
    static let opacityContainerSpinner = 0.3
    
    static let widthScreen = UIScreen.main.bounds.width
    static let containerDetailPadding: CGFloat = -15
    static let smallPadding: CGFloat = 5
    static let padding: CGFloat = 10
    static let doublePadding: CGFloat = 20
    static let multiplierTwo: CGFloat = 2
    static let defaultCornerRadius: CGFloat = 10
    
    static let defaultHeightContainerCell: CGFloat = 100
    static let defaultCellCornerRadius: CGFloat = 10
    static let defaultHeightCell = 44
}
