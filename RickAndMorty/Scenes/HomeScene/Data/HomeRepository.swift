//
//  HomeRepository.swift
//  RickAndMorty
//
//  Created by Tomas moran on 03/10/2023.
//

import UIKit

struct HomeRepository: HomeRepositoryProtocol {
    
    let apiClientService: ApiClientServiceProtocol
    let urlString: String
    
    func fetchData() async throws -> [HomeItem] {
        let url = URL(string: urlString)
        return try await apiClientService.request(url: url, type: HomeDTO.self).toDomain()
    }
    
}
