//
//  LocationsCoordinator.swift
//  RickAndMorty
//
//  Created by Tomas moran on 13/10/2023.
//

import UIKit

final class LocationsCoordinator: Coordinator {
    
    private let locationsFactory: LocationsFactoryProtocol
    var navigation: UINavigationController
    
    init(
        locationsFactory: LocationsFactoryProtocol,
        navigation: UINavigationController
    ) {
        self.locationsFactory = locationsFactory
        self.navigation = navigation
    }
    
    func start() {
        let controller = locationsFactory.makeModule()
        navigation.pushViewController(controller, animated: true)
        navigation.navigationBar.prefersLargeTitles = true
    }
}
