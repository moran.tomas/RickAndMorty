//
//  Reusable.swift
//  RickAndMorty
//
//  Created by Tomas moran on 03/10/2023.
//

protocol Reusable {}

extension Reusable {
    static var reuseIdentifier: String { String(describing: self) }
}
