//
//  Endpoint.swift
//  RickAndMorty
//
//  Created by Tomas moran on 04/10/2023.
//

struct Endpoint {
    static let baseUrl = "https://rickandmortyapi.com/api"
}
