//
//  HomeRepository.swift
//  RickAndMorty
//
//  Created by Tomas moran on 03/10/2023.
//

protocol HomeRepositoryProtocol {
    func fetchData() async throws -> [HomeItem]
}
