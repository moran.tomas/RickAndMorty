//
//  AppContainer.swift
//  RickAndMorty
//
//  Created by Tomas moran on 11/10/2023.
//

protocol AppContainerProtocol {
    var apiClient: ApiClientServiceProtocol & RemoteImageDataServiceProtocol { get }
    var localDataService: LocalDataImageServiceProtocol { get }
    func getDataImageUseCase() -> ImageDataUseCaseProtocol
}

struct AppContainer: AppContainerProtocol {
    
    var apiClient: ApiClientServiceProtocol & RemoteImageDataServiceProtocol = ApiClientService()
    var localDataService: LocalDataImageServiceProtocol = LocalDataImageService()
    
    func getDataImageUseCase() -> ImageDataUseCaseProtocol {
        let imageDataRepository = ImageDataRepository(
            remoteDataService: apiClient,
            localDataCache: localDataService)
        return ImageDataUseCase(imageDataRepository: imageDataRepository)
    }
    
    
}
