//
//  HomeViewController.swift
//  RickAndMorty
//
//  Created by Tomas moran on 14/07/2023.
//

import UIKit
import Combine

protocol HomeViewControllerCoordinator: AnyObject {
    func didSelectCell(model: HomeItem)
}

class HomeViewController: UICollectionViewController {

    private let viewModel: HomeViewModelProtocol
    private var cancellable = Set<AnyCancellable>()
    private weak var coordinator: HomeViewControllerCoordinator?
    
    init(
        viewModel: HomeViewModelProtocol,
        layout: UICollectionViewFlowLayout,
        coordinator: HomeViewControllerCoordinator) {
            self.viewModel = viewModel
            self.coordinator = coordinator
            super.init(collectionViewLayout: layout)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configuI()
        configCollectionView()
        stateController()
        viewModel.viewDidLoad()
    }
    
    private func stateController() {
        viewModel
            .state
            .receive(on: RunLoop.main)
            .sink { [weak self] state in
                self?.hideSpinner()
                switch state {
                case .success:
                    self?.collectionView.reloadData()
                    
                case .loading:
                    self?.showSpinner()
                    
                case .fail(let error):
                    self?.presentAlert(message: error, title: Constants.error)
                }
            }.store(in: &cancellable)
    }

    private func configuI() {
        view.backgroundColor = .systemBackground
    }
    
    private func configCollectionView() {
        collectionView.register(ItemHomeViewCell.self, forCellWithReuseIdentifier: ItemHomeViewCell.reuseIdentifier)
    }
    
}

extension HomeViewController {
    
    override func collectionView(
        _ collectionView: UICollectionView,
        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            guard
                let cell = collectionView.dequeueReusableCell(
                    withReuseIdentifier: ItemHomeViewCell.reuseIdentifier,
                    for: indexPath) as? ItemHomeViewCell
            else { return UICollectionViewCell() }
            
            let viewModel = viewModel.getItemMenuViewModel(indexPath: indexPath)
            cell.configData(viewModel: viewModel)
            return cell
        }
    
    override func collectionView(
        _ collectionView: UICollectionView,
        numberOfItemsInSection section: Int) -> Int {
            viewModel.menuItemsCount
    }
    
}

extension HomeViewController {
    override func collectionView(
        _ collectionView: UICollectionView,
        didSelectItemAt indexPath: IndexPath) {
            let model = viewModel.getMenuItem(indexPath: indexPath)
            coordinator?.didSelectCell(model: model)
    }
}

extension HomeViewController: SpinnerDisplayable { }

extension HomeViewController: MessageDisplayable { }
