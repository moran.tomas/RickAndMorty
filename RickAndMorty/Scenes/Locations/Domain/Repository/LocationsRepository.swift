//
//  LocationsRepository.swift
//  RickAndMorty
//
//  Created by Tomas moran on 13/10/2023.
//

import UIKit

protocol LocationsRepositoryProtocol {
    func fetchLocations(
        urlLocations: String
    ) async throws -> (info: Info, locations: [Location])
}

struct LocationsRepository: LocationsRepositoryProtocol {
    
    private(set) var remoteService: ApiClientServiceProtocol
    
    func fetchLocations(
        urlLocations: String
    ) async throws -> (info: Info, locations: [Location]) {
        let url = URL(string: urlLocations)
        return try await remoteService
            .request(url: url, type: ResultLocationsDTO.self)
            .toDomain()
    }
}
