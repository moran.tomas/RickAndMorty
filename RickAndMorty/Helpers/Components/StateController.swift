//
//  StateController.swift
//  RickAndMorty
//
//  Created by Tomas moran on 03/10/2023.
//

enum StateController {
    case success
    case loading
    case fail(error: String)
}
