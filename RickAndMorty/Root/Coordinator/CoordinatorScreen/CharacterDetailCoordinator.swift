//
//  CharacterDetailCoordinator.swift
//  RickAndMorty
//
//  Created by Tomas moran on 11/10/2023.
//

import UIKit

final class CharacterDetailCoordinator: Coordinator {
    
    var navigation: UINavigationController
    private var characterDetailFactory: CharacterDetailFactoryProtocol
    
    init(
        navigation: UINavigationController,
        characterDetailFactory: CharacterDetailFactoryProtocol
    ) {
        self.navigation = navigation
        self.characterDetailFactory = characterDetailFactory
    }
    
    func start() {
        let controller = characterDetailFactory.makeModule(coordinator: self)
        navigation.pushViewController(controller, animated: true)
    }
    
}

extension CharacterDetailCoordinator: CharacterDetailViewControllerCoordinator {
    
    func didTapOriginButton() {
        let originCoordinator = characterDetailFactory
            .makeOriginCoordinator(navigation: navigation)
        originCoordinator.start()
    }
    
    func didTapLocationButton() {
        let locationCoordinator = characterDetailFactory
            .makeLocationCoordinator(navigation: navigation)
        locationCoordinator.start()
    }
        
}
