//
//  ResultEpisodesDTO.swift
//  RickAndMorty
//
//  Created by Tomas moran on 14/10/2023.
//

import Foundation

struct ResultEpisodesDTO: Decodable {
    let info: InfoDTO
    let results: [EpisodeDTO]
}
