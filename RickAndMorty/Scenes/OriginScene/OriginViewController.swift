//
//  OriginViewController.swift
//  RickAndMorty
//
//  Created by Tomas moran on 13/10/2023.
//

import UIKit

final class OriginViewController: UIViewController {
    // MARK: - Public properties
    
    // MARK: - Private properties
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground
    }
    
    // MARK: - Helpers
    
    // MARK: - Actions
    
}

// MARK: - Extensions here
