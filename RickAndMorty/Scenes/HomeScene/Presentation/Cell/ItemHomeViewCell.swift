//
//  ItemHomeViewCell.swift
//  RickAndMorty
//
//  Created by Tomas moran on 03/10/2023.
//

import UIKit

final class ItemHomeViewCell: UICollectionViewCell, Reusable {
    
    private let container: UIView = {
        let view = UIView()
        view.backgroundColor = .systemGroupedBackground
        view.layer.cornerRadius = ViewValues.defaultCornerRadius
        view.layer.masksToBounds = true
        return view
    }()
    
    private let categoryImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: Images.defaultImage)
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    private let categoryLabel: UILabel = {
       let label = UILabel()
        label.textColor = .white
        label.text = "Category"
        label.font = UIFont.preferredFont(forTextStyle: .headline)
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Helpers
    private func configUI() {
        addSubview(container)
        container.fillSuperView(widthPadding: ViewValues.padding)
        
        container.addSubview(categoryImageView)
        categoryImageView.fillSuperView()
        
        configGradientFortTitle()
        
        container.addSubview(categoryLabel)
        categoryLabel.setConstraints(
            right: container.rightAnchor,
            bottom: container.bottomAnchor,
            left: container.leftAnchor,
            pRight: ViewValues.padding,
            pBottom: ViewValues.padding,
            pLeft: ViewValues.padding)
        
    }
    
    private func configGradientFortTitle() {
        let gradientMaskLayer = CAGradientLayer()
        gradientMaskLayer.frame = self.bounds
        gradientMaskLayer.colors = [UIColor.clear.cgColor, UIColor.darkGray.cgColor]
        gradientMaskLayer.locations = [0.2, 0.9]
        container.layer.addSublayer(gradientMaskLayer)
    }
    
    func configData(viewModel: ItemHomeViewModel) {
        categoryLabel.text = viewModel.title
        categoryImageView.image = UIImage(named: viewModel.imageName)
    }
    
}

