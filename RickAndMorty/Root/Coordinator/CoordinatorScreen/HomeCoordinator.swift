//
//  HomeCoordinator.swift
//  RickAndMorty
//
//  Created by Tomas moran on 02/10/2023.
//

import UIKit

final class HomeCoordinator: Coordinator {
    
    var navigation: UINavigationController
    private let homeFactory: HomeFactory!
    
    init(navigation: UINavigationController, homeFactory: HomeFactory) {
        self.navigation = navigation
        self.homeFactory = homeFactory
    }
    
    func start() {
        let controller = homeFactory.createView(coordinator: self)
        navigation.pushViewController(controller, animated: true)
    }
    
}

extension HomeCoordinator: HomeViewControllerCoordinator {
    func didSelectCell(model: HomeItem) {
        switch model.title {
        case "characters": 
            goToCharacters(
                urlList: model.url)
            
        case "locations":
            goToLocations(
                urlLocations: model.url)
            
        case "episodes":
            goToEpisodes(urlEpisodes: model.url)
            
        default:
            break
        }
    }
    
    private func goToCharacters(urlList: String) {
        let characterCoordinator = homeFactory.makeCharactersCoordinator(
            navigation: navigation,
            urlList: urlList)
        characterCoordinator.start()
    }
    
    private func goToLocations(urlLocations: String) {
        let locationsCoordinator = homeFactory.makeLocationsCoordinator(
            navigation: navigation,
            urlLocation: urlLocations)
        locationsCoordinator.start()
    }
    
    private func goToEpisodes(urlEpisodes: String) {
        let episodesCoordinator = homeFactory
            .makeEpisodesCoordinator(
                navigation: navigation,
                urlEpisodes: urlEpisodes)
        episodesCoordinator.start()
    }
    
}
