//
//  ItemCharacterCell.swift
//  RickAndMorty
//
//  Created by Tomas moran on 04/10/2023.
//

import UIKit

final class ItemCharacterCell: UITableViewCell {
    
    private let containerView: UIView = {
        let view = UIView()
        view.backgroundColor = .tertiarySystemGroupedBackground
        view.setHeightConstraints(with: ViewValues.defaultHeightContainerCell)
        view.layer.cornerRadius = ViewValues.defaultCellCornerRadius
        view.layer.masksToBounds = true
        return view
    }()
    
    private let characterImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.setWidthConstraints(with: ViewValues.defaultHeightContainerCell)
//        imageView.image = UIImage(named: "default")
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    private let labelContainerStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fillEqually
        return stackView
    }()
    
    private let nameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.preferredFont(
            forTextStyle: .headline,
            compatibleWith: UITraitCollection(legibilityWeight: .bold))
        label.textColor = .systemBlue
        label.text = "rick"
        return label
    }()
    
    private let specieLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.preferredFont(forTextStyle: .callout)
        label.text = "Human"
        return label
    }()
    
    private let statusLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.preferredFont(forTextStyle: .callout)
        label.text = "🦍"
        return label
    }()
    
    private var task: Task<Void, Never>?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configUI()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        task?.cancel()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init coder not been implemented")
    }
    
    private func configUI() {
        addSubview(containerView)
        containerView.setConstraints(
            top: topAnchor,
            right: rightAnchor,
            bottom: bottomAnchor,
            left: leftAnchor,
            pRight: ViewValues.doublePadding,
            pBottom: ViewValues.doublePadding,
            pLeft: ViewValues.doublePadding)
        
        containerView.addSubview(characterImageView)
        characterImageView.setConstraints(
            top: containerView.topAnchor,
            bottom: containerView.bottomAnchor,
            left: containerView.leftAnchor)
        
        containerView.addSubview(labelContainerStackView)
        labelContainerStackView.setConstraints(
            top: containerView.topAnchor,
            right: containerView.rightAnchor,
            bottom: containerView.bottomAnchor,
            left: characterImageView.rightAnchor,
            pTop: ViewValues.padding,
            pBottom: ViewValues.padding,
            pLeft: ViewValues.padding)
        
        [nameLabel, specieLabel, statusLabel].forEach {
            labelContainerStackView.addArrangedSubview($0)
        }
    }
    
    func configData(viewModel: ItemCharacterViewModel) {
        nameLabel.text = viewModel.name
        specieLabel.text = viewModel.specie
        statusLabel.text = viewModel.status
        setImage(viewModel: viewModel)
    }
    
    private func setImage(viewModel: ItemCharacterViewModel) {
        characterImageView.addDefaultImage()
        
        if let data = viewModel.imageData {
            characterImageView.setImageFromData(data: data)
            
        } else {
            task = Task {
                let dataImage = await viewModel.getImageData()
                characterImageView.setImageFromData(data: dataImage)
            }
        }
    }
    
}

extension ItemCharacterCell: Reusable { }
