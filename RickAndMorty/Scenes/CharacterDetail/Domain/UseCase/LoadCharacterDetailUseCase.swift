//
//  LoadCharacterDetailUseCase.swift
//  RickAndMorty
//
//  Created by Tomas moran on 11/10/2023.
//

protocol LoadCharacterDetailUseCaseProtocol {
    func execute() async throws -> Character
}

struct LoadCharacterDetailUseCase: LoadCharacterDetailUseCaseProtocol {
    
    private(set) var characterDetailRepository: CharacterDetailRepositoryProtocol
    private(set) var urlDetail: String
    
    func execute() async throws -> Character {
        try await characterDetailRepository.fetchCharacterDetail(
            urlDetail: urlDetail)
    }
    
}
