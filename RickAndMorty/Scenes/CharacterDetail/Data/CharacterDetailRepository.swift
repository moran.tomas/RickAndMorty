//
//  CharacterDetailRepository.swift
//  RickAndMorty
//
//  Created by Tomas moran on 11/10/2023.
//

import Foundation

struct CharacterDetailRepository: CharacterDetailRepositoryProtocol {
    
    private(set) var remoteService: ApiClientServiceProtocol
    
    func fetchCharacterDetail(urlDetail: String) async throws -> Character {
        let url = URL(string: urlDetail)
        let result = try await remoteService.request(
            url: url,
            type: CharacterDTO.self)
        return result.toDomain()
    }
    
}
