//
//  HomeViewModel.swift
//  RickAndMorty
//
//  Created by Tomas moran on 03/10/2023.
//

import Combine
import Foundation

protocol HomeViewModelProtocol {
    var state: PassthroughSubject<StateController, Never> { get }
    var menuItemsCount: Int { get }
    func viewDidLoad()
    func getItemMenuViewModel(indexPath: IndexPath) -> ItemHomeViewModel
    func getMenuItem(indexPath: IndexPath) -> HomeItem
}

final class HomeViewModel: HomeViewModelProtocol {
    
    var state: PassthroughSubject<StateController, Never>
    private let loadHomeCase: LoadHomeCaseProtocol
    private var homeItems: [HomeItem] = []
    
    var menuItemsCount: Int {
        homeItems.count
    }
    
    init(state: PassthroughSubject<StateController, Never>, loadHomeCase: LoadHomeCaseProtocol) {
        self.state = state
        self.loadHomeCase = loadHomeCase
    }
    
    func viewDidLoad() {
        state.send(.loading)
        Task {
            let result = await loadHomeCase.execute()
            updateUI(result: result)
        }
    }
    
    private func updateUI(result: Result<[HomeItem], Error>) {
        switch result {
        case .success(let items):
            self.homeItems = items
            state.send(.success)
            
        case .failure(let error):
            state.send(.fail(error: error.localizedDescription))
        }
    }
    
    func getItemMenuViewModel(indexPath: IndexPath) -> ItemHomeViewModel {
        let homeItems = homeItems[indexPath.row]
        return ItemHomeViewModel(menuItem: homeItems)
    }
    
    func getMenuItem(indexPath: IndexPath) -> HomeItem {
        homeItems[indexPath.row]
    }
    
}
