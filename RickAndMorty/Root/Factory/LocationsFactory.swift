//
//  LocationsFactory.swift
//  RickAndMorty
//
//  Created by Tomas moran on 13/10/2023.
//

import UIKit
import Combine

protocol LocationsFactoryProtocol {
    func makeModule() -> UIViewController
}

struct LocationsFactory: LocationsFactoryProtocol {
    
    private(set) var urlLocations: String
    private(set) var appContainer: AppContainerProtocol
    
    func makeModule() -> UIViewController {
        let state = PassthroughSubject<StateController, Never>()
        
        let locationRepository = LocationsRepository(
            remoteService: appContainer.apiClient)
        
        let loadLocationUseCase = LoadLocationsUseCase(
            locationsRepository: locationRepository,
            urlLocations: urlLocations)
        let lastPageUseCase = LastPageValidationUseCase()
        
        let viewModel = LocationViewModel(
            state: state,
            loadLocationUseCase: loadLocationUseCase,
            lastPageUseCase: lastPageUseCase)
        
        let controller = LocationsViewController(
            viewModel: viewModel)
        controller.title = Constants.locations
        return controller
    }
}
