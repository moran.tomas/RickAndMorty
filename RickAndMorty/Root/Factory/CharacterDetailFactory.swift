//
//  CharacterDetailFactory.swift
//  RickAndMorty
//
//  Created by Tomas moran on 11/10/2023.
//

import UIKit
import Combine

protocol CharacterDetailFactoryProtocol {
    func makeModule(coordinator: CharacterDetailViewControllerCoordinator) -> UIViewController
    func makeOriginCoordinator(navigation: UINavigationController) -> Coordinator
    func makeLocationCoordinator(navigation: UINavigationController) -> Coordinator
}

struct CharacterDetailFactory: CharacterDetailFactoryProtocol {
    
    let urlDetail: String
    let appContainer: AppContainerProtocol
    
    func makeModule(coordinator: CharacterDetailViewControllerCoordinator) -> UIViewController {
        let state = PassthroughSubject<StateController, Never>()
        let characterDetailRepository = CharacterDetailRepository(
            remoteService: appContainer.apiClient)
        let loadCharacterDetailUseCase = LoadCharacterDetailUseCase(
            characterDetailRepository: characterDetailRepository,
            urlDetail: urlDetail)
        let viewModel = CharacterDetailViewModel(
            state: state,
            loadCharacterDetailUseCase:
                loadCharacterDetailUseCase,
            dataImageUseCase: appContainer.getDataImageUseCase())
        let controller = CharacterDetailViewController(
            viewModel: viewModel,
            coordinator: coordinator)
        return controller
    }
    
    func makeOriginCoordinator(navigation: UINavigationController) -> Coordinator {
        let factory = OriginFactory()
        let coordinator = OriginCoordinator(
            navigation: navigation,
            originFactory: factory)
        return coordinator
    }
    
    func makeLocationCoordinator(navigation: UINavigationController) -> Coordinator {
        let factory = LocationDetailFactory()
        let coordinator = LocationDetailCoordinator(
            navigation: navigation,
            locationDetailFactory: factory)
        return coordinator
    }
    
}
